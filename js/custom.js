$(document).ready(function () {
    $(".owl-carousel").owlCarousel(
        {
            loop: true,
            dots: false,
            nav: true,
            navText: ["<span class='material-icons'>arrow_back_ios</span>", "<span class='material-icons'>arrow_forward_ios</span>"],
            autoplay:true,
            responsiveClass:true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1,
                    loop: true,
                },
                // breakpoint from 480 up
                480: {
                    items: 2,  
                    loop: true,              
                },
                // breakpoint from 768 up
                768: {
                    items: 3, 
                    loop: true,                 
                },
                // breakpoint from 768 up
                1024: {
                    items: 3, 
                    loop: true,              
                },
                  // breakpoint from 1200 up
                1200: {
                    items: 5, 
                    loop: true,              
                }
            },
           
        }
    );
     $(function () {
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll >= 150) {
                    $(".bsnav-light").addClass("bg-onscroll");
                } else {
                    $(".bsnav-light").removeClass("bg-onscroll");
                }
            });
        });
     AOS.init();
});



